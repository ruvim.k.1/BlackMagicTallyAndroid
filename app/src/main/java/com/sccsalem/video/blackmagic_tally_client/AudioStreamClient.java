package com.sccsalem.video.blackmagic_tally_client;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 * Created by Ruvim on 8/16/2017.
 */

public class AudioStreamClient {
	private static final String TAG = "AudioStreamClient";
	
	private Socket socket = null;
	protected InputStream inputStream = null;
	private String host = "";
	protected int port = 50513; 
	boolean mDisconnectCalled = false;
	
	AudioPlayer mAudioPlayer = new AudioPlayer ();
	
	public AudioStreamClient (String host) {
		this.host = host;
	}
	public void connect () throws IOException {
		mDisconnectCalled = false;
		if (socket == null) {
			socket = new Socket (host, port);
			inputStream = socket.getInputStream ();
		} else {
			if (inputStream != null)
				inputStream.close ();
			socket.close ();
			socket = new Socket (host, port);
			inputStream = socket.getInputStream ();
		}
		mAudioPlayer.init ();
	}
	public void disconnect () throws IOException {
		mDisconnectCalled = true;
		if (socket != null) {
			if (socket.isConnected ()) {
				inputStream.close ();
				socket.close ();
			}
		}
		mAudioPlayer.close ();
	} 
	public boolean tryReconnect () {
		try {
			connect ();
		} catch (IOException err) {
			err.printStackTrace ();
		}
		return socket.isConnected ();
	}
	public boolean isConnected () {
		return socket != null && socket.isConnected ();
	}
	private void sleep (long ms) {
		try {
			Thread.sleep (ms);
		} catch (InterruptedException err) {
			err.printStackTrace ();
		}
	} 
	public void resetPlayer (int new_sample_rate) { 
		mAudioPlayer.SAMPLE_RATE = new_sample_rate; 
		mAudioPlayer.reset (); 
	} 
	public void play () throws IOException {
		if (inputStream == null) return; 
		byte [] buffer = new byte [1024]; 
		do {
			int bRead = inputStream.read (buffer, 0, buffer.length); // Read. 
			if (bRead <= 0) { // No data read. 
				if (bRead < 0) { // Some sort of error (connection lost?): 
					if (!tryReconnect ())
						sleep (100); // Wait a little if couldn't connect. 
				}
				continue; 
			}
			mAudioPlayer.play (buffer, bRead); // Play. 
		} while (!mDisconnectCalled); // Continue until disconnect () called. 
	}
} 

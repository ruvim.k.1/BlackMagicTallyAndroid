package com.sccsalem.video.blackmagic_tally_client;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenActivity extends AppCompatActivity {
	private static final String TAG = "BlackMagicTally"; 
	/**
	 * Whether or not the system UI should be auto-hidden after
	 * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
	 */
	private static final boolean AUTO_HIDE = true;
	
	/**
	 * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
	 * user interaction before hiding the system UI.
	 */
	private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
	
	/**
	 * Some older devices needs a small delay between UI widget updates
	 * and a change of the status and navigation bar.
	 */
	private static final int UI_ANIMATION_DELAY = 300;
	private final Handler mHideHandler = new Handler ();
	private View mContentView;
	private View mStatusView; 
	private Button mPTTButton; 
	private final Runnable mHidePart2Runnable = new Runnable () {
		@SuppressLint("InlinedApi")
		@Override
		public void run () {
			// Delayed removal of status and navigation bar
			
			// Note that some of these constants are new as of API 16 (Jelly Bean)
			// and API 19 (KitKat). It is safe to use them, as they are inlined
			// at compile-time and do nothing on earlier devices.
			mContentView.setSystemUiVisibility (View.SYSTEM_UI_FLAG_LOW_PROFILE
					| View.SYSTEM_UI_FLAG_FULLSCREEN
					| View.SYSTEM_UI_FLAG_LAYOUT_STABLE
					| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
					| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
					| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		}
	};
	private View mControlsView;
	private final Runnable mShowPart2Runnable = new Runnable () {
		@Override
		public void run () {
			// Delayed display of UI elements
			ActionBar actionBar = getSupportActionBar ();
			if (actionBar != null) {
				actionBar.show ();
			}
			mControlsView.setVisibility (View.VISIBLE);
		}
	};
	private boolean mVisible;
	private final Runnable mHideRunnable = new Runnable () {
		@Override
		public void run () {
			hide ();
		}
	};
	/**
	 * Touch listener to use for in-layout UI controls to delay hiding the
	 * system UI. This is to prevent the jarring behavior of controls going away
	 * while interacting with activity UI.
	 */
	private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener () {
		@Override
		public boolean onTouch (View view, MotionEvent motionEvent) {
			if (AUTO_HIDE) {
				delayedHide (AUTO_HIDE_DELAY_MILLIS);
			}
			return false;
		}
	};
	
//	TallyClient mClient = null; 
//	AudioStreamClient mSoundClient = null; 
	Thread mListenChanges = null; 
	
	BroadcastReceiver mTallyReceiver = new BroadcastReceiver () { 
		@Override public void onReceive (Context context, Intent intent) { 
			if (intent.hasExtra (TallyStreamService.DATA_MESSAGE)) 
				lastMessage = intent.getByteArrayExtra (TallyStreamService.DATA_MESSAGE); 
//			else lastMessage = null; 
			runOnUiThread (mUpdateViews); 
		} 
	}; 
	
	SharedPreferences mPrefs = null; 
	
	private String getPrefHost () { 
		return mPrefs != null ? mPrefs.getString ("host", getString (R.string.pref_host_default)) 
				: getString (R.string.pref_host_default); 
	} 
	private int getPrefCam () { 
		int defValue = Integer.valueOf (getString (R.string.pref_cam_default)); 
		return mPrefs != null ? mPrefs.getInt ("cam", defValue) : defValue; 
	} 
	private byte [] lastMessage = null; 
	Runnable mUpdateViews = new Runnable () { 
		@Override public void run () { 
			if (mBtnCam != null) 
				mBtnCam.setText (getString (R.string.btn_change_cam) 
				.replace ("{cam}", String.valueOf (getPrefCam ()))); 
//			if (mClient == null || !mClient.isConnected () || !mConnected) { 
//				if (mContentView instanceof TextView) { 
//					((TextView) mContentView).setText (R.string.status_connecting); 
//					((TextView) mContentView).setTextColor (getResources () 
//						.getColor (R.color.status_connecting)); 
//				} 
//				return; 
//			} 
			byte [] msg = lastMessage; 
			if (msg == null) {
				if (mStatusView instanceof TextView) {
					((TextView) mStatusView).setText (R.string.status_connecting); 
					((TextView) mStatusView).setTextColor (getResources () 
						.getColor (R.color.status_connecting)); 
				} 
				return; 
			} 
			int resTextId = R.string.tally_none; 
			int resColorId = R.color.tally_none; 
			int cam = getPrefCam (); 
			if (msg[0] == 2 && (cam == msg[1] || cam == msg[2])) { 
				resTextId = R.string.tally_transit; 
				resColorId = R.color.tally_transit; 
			} else if (msg[0] == 1) { 
				if (cam == msg[1]) { 
					resTextId = R.string.tally_program; 
					resColorId = R.color.tally_program; 
				} else if (cam == msg[2]) { 
					resTextId = R.string.tally_preview; 
					resColorId = R.color.tally_preview; 
				} 
			} else if (msg[0] == 5&& cam == msg[2]) { 
				resTextId = R.string.tally_getready; 
				resColorId = R.color.tally_getready; 
			} 
			if (mStatusView instanceof TextView) { 
				((TextView) mStatusView).setText (resTextId); 
				((TextView) mStatusView).setTextColor (getResources () 
						.getColor (resColorId)); 
			} 
		} 
	}; 
	
	boolean mActivityPaused = false; 
	private void disconnect () {
//		if (mClient != null) 
//			mClient.disconnect (); 
//		if (mSoundClient != null) try {
//			mSoundClient.disconnect ();
//		} catch (IOException err) {
//			err.printStackTrace (); 
//		} 
		LocalBroadcastManager.getInstance (this) 
				.unregisterReceiver (mTallyReceiver); 
		stopService (new Intent (this, TallyStreamService.class)); 
		unbindService (mTallyServiceConnection); 
		stopService (new Intent (this, SoundStreamService.class)); 
		unbindService (mServiceConnection); 
	} 
	@Override public void onPause () { 
		mActivityPaused = true; 
		disconnect (); 
		stopTalkingBack (); // Just in case it hasn't stopped yet. 
		super.onPause (); 
	} 
	@Override public void onResume () { 
		super.onResume (); 
		mActivityPaused = false; 
		reconnect (); 
	} 
	
	boolean mConnected = false; 
	ServiceConnection mServiceConnection = new ServiceConnection () {
		@Override
		public void onServiceConnected (ComponentName componentName, IBinder iBinder) {
			Log.d (TAG, "Service bound: " + componentName); 
		}
		
		@Override
		public void onServiceDisconnected (ComponentName componentName) {
			Log.d (TAG, "Service unbound: " + componentName); 
		}
	}; 
	ServiceConnection mTallyServiceConnection = new ServiceConnection () {
		@Override
		public void onServiceConnected (ComponentName componentName, IBinder iBinder) {
			
		}
		
		@Override
		public void onServiceDisconnected (ComponentName componentName) {
			
		}
	}; 
	ServiceConnection mTalkBackServiceConnection = new ServiceConnection () {
		@Override
		public void onServiceConnected (ComponentName componentName, IBinder iBinder) {
			
		} 
		@Override
		public void onServiceDisconnected (ComponentName componentName) {
			
		} 
	}; 
	private void reconnect () { 
//		if (mClient != null) mClient.disconnect (); 
//		mClient = new TallyClient (getPrefHost ()); 
//		mSoundClient = new AudioStreamClient (getPrefHost ()); 
//		mListenChanges = (new Thread () { 
//			@Override public void run () { 
//				byte [] msg = new byte [0]; 
//				while (!mActivityPaused && !mClient.mDisconnectCalled) { 
////					while ((msg == null || !mClient.isConnected ()) && 
////							!mActivityPaused && !mClient.mDisconnectCalled) { 
////						runOnUiThread (mUpdateViews); 
////						Log.i (TAG, "Connecting ... "); 
////						mClient.connect (); 
////						if (!mClient.isConnected ()) 
////							try { 
////								sleep (1000); 
////							} catch (InterruptedException err) { 
////								err.printStackTrace (); 
////							} 
////						else msg = new byte [0]; 
////					} 
//					Log.i (TAG, "Connected. "); 
//					runOnUiThread (mUpdateViews); 
//					msg = new byte [4]; 
//					int bRead; 
//					int retries = 0; 
//					while (retries < 8) { 
//						Log.d (TAG, "Trying to connect ... "); 
//						mConnected = false; 
//						runOnUiThread (mUpdateViews); 
//						try { 
//							mClient.connect (); 
//							mConnected = mClient.isConnected (); 
//							if (!mConnected) continue; 
//							while ((bRead = mClient.inputStream.read (msg, 0, 4)) > 0) { 
//								if (bRead < 0) break; 
//								if (bRead == 4) { 
//									lastMessage = msg; 
//									runOnUiThread (mUpdateViews); 
//								} 
//								if (retries > 0) retries = 0; 
//							} 
//							retries++; 
//						} catch (IOException err) { 
//							err.printStackTrace (); 
//							retries++; 
//						} 
//					} 
////					while ((msg = mClient.readMessage ()) != null) { 
////						lastMessage = msg; 
////						runOnUiThread (mUpdateViews); 
////					} 
//					Log.i (TAG, "Disconnected. "); 
//					mConnected = false; 
//				} 
//				mClient.disconnect (); 
//			} 
//		}); 
//		mListenChanges.start (); 
//		(new AsyncTask<Void,Void,Void> () { 
//			@Override protected Void doInBackground (Void ... params) { 
//				try { 
//					mSoundClient.connect (); 
//					mSoundClient.play (); 
//				} catch (IOException err) { 
//					err.printStackTrace (); 
//				} 
//				return null; 
//			} 
//			@Override protected void onPreExecute () { 
//				
//			} 
//			@Override protected void onPostExecute (Void result) { 
//				
//			} 
//			@Override protected void onProgressUpdate (Void ... values) { 
//				
//			} 
//		}).execute ();
		Intent readTally = new Intent (this, TallyStreamService.class); 
		readTally.putExtra ("host", getPrefHost ()); 
		bindService (readTally, mTallyServiceConnection, BIND_AUTO_CREATE); 
		startService (readTally);
		IntentFilter mTallyFilter = new IntentFilter (TallyStreamService.BROADCAST_UPDATE); 
		LocalBroadcastManager.getInstance (this) 
				.registerReceiver (mTallyReceiver, mTallyFilter); 
		Intent playStream = new Intent (this, SoundStreamService.class); 
		playStream.putExtra ("host", getPrefHost ()); 
		bindService (playStream, mServiceConnection, BIND_AUTO_CREATE); 
		startService (playStream); 
	} 
	boolean isTalkingBack = false; 
	void startTalkingBack () { 
		if (isTalkingBack) return; 
		isTalkingBack = true; 
		Intent talkBack = new Intent (this, TalkBackService.class); 
		talkBack.putExtra ("host", getPrefHost ()); 
		bindService (talkBack, mTalkBackServiceConnection, BIND_AUTO_CREATE); 
		startService (talkBack); 
		if (mPTTButton != null) 
			mPTTButton.setBackgroundColor (getResources ().getColor (R.color.ptt_bg_talk)); 
	} 
	void stopTalkingBack () { 
		if (!isTalkingBack) return; 
		isTalkingBack = false; 
		stopService (new Intent (this, TalkBackService.class)); 
		unbindService (mTalkBackServiceConnection); 
		if (mPTTButton != null) 
			mPTTButton.setBackgroundColor (getResources ().getColor (R.color.ptt_bg_none)); 
	} 
	
	Button mBtnCam = null; 
	Button mBtnIp = null; 
	
	private void userEditCamNumber () { 
		final EditText editText = (EditText) getLayoutInflater ().inflate (R.layout.edit_cam, 
				(ViewGroup) findViewById (R.id.vRoot), false); 
		editText.setText (String.valueOf (getPrefCam ())); 
		AlertDialog dialog = new AlertDialog.Builder (this) 
				.setTitle (R.string.title_cam_number) 
				.setView (editText) 
				.setPositiveButton (R.string.label_ok, new DialogInterface.OnClickListener () { 
					@Override public void onClick (DialogInterface dialogInterface, int i) { 
						int number; 
						try { 
							number = Integer.valueOf (editText.getText ().toString ()); 
						} catch (NumberFormatException err) { 
							return; 
						} 
						SharedPreferences.Editor editor = mPrefs.edit (); 
						editor.putInt ("cam", number); 
						editor.commit (); 
						runOnUiThread (mUpdateViews); 
					} 
				}) 
				.setNegativeButton (R.string.label_cancel, null) 
				.create (); 
		dialog.show (); 
	} 
	private void userEditIpAddress () {
		final EditText editText = (EditText) getLayoutInflater ().inflate (R.layout.edit_ip, 
				(ViewGroup) findViewById (R.id.vRoot), false); 
		editText.setText (getPrefHost ()); 
		AlertDialog dialog = new AlertDialog.Builder (this) 
				.setTitle (R.string.title_server_ip) 
				.setView (editText) 
				.setPositiveButton (R.string.label_ok, new DialogInterface.OnClickListener () { 
					@Override public void onClick (DialogInterface dialogInterface, int i) { 
						SharedPreferences.Editor editor = mPrefs.edit (); 
						editor.putString ("host", editText.getText ().toString ()); 
						editor.commit (); 
						disconnect (); 
						reconnect (); 
						runOnUiThread (mUpdateViews); 
					} 
				}) 
				.setNegativeButton (R.string.label_cancel, null) 
				.create (); 
		dialog.show (); 
	} 
	
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate (savedInstanceState); 
		
		setContentView (R.layout.activity_fullscreen);
		
		mVisible = true;
		mControlsView = findViewById (R.id.fullscreen_content_controls);
		mContentView = findViewById (R.id.fullscreen_content); 
		
		mStatusView = findViewById (R.id.status_text); 
		mPTTButton = (Button) findViewById (R.id.ptt_button); 
		mPTTButton.setOnTouchListener (new View.OnTouchListener () { 
			@Override public boolean onTouch (View view, MotionEvent motionEvent) { 
				switch (motionEvent.getAction ()) { 
					case MotionEvent.ACTION_DOWN: 
						startTalkingBack (); 
						break; 
					case MotionEvent.ACTION_UP: 
					case MotionEvent.ACTION_CANCEL: 
						stopTalkingBack (); 
						break; 
				} 
				return true; 
			} 
		}); 
		mPTTButton.setVisibility (View.GONE); 
		
		mPrefs = getSharedPreferences (getString (R.string.client_pref), MODE_PRIVATE); 
		
		mBtnCam = (Button) findViewById (R.id.btn_change_cam); 
		mBtnIp = (Button) findViewById (R.id.btn_change_ip); 
		mBtnCam.setOnClickListener (new View.OnClickListener () { 
			@Override public void onClick (View view) { 
				userEditCamNumber (); 
			} 
		}); 
		mBtnIp.setOnClickListener (new View.OnClickListener () { 
			@Override public void onClick (View view) { 
				userEditIpAddress (); 
			} 
		}); 
		
		reconnect (); 
		mUpdateViews.run (); 
		
		// Set up the user interaction to manually show or hide the system UI.
		mContentView.setOnClickListener (new View.OnClickListener () {
			@Override
			public void onClick (View view) {
				toggle ();
			}
		}); 
		
		// Upon interacting with UI controls, delay any scheduled hide()
		// operations to prevent the jarring behavior of controls going away
		// while interacting with the UI.
//		findViewById (R.id.dummy_button).setOnTouchListener (mDelayHideTouchListener);
	}
	
	@Override
	protected void onPostCreate (Bundle savedInstanceState) {
		super.onPostCreate (savedInstanceState);
		
		// Trigger the initial hide() shortly after the activity has been
		// created, to briefly hint to the user that UI controls
		// are available.
		delayedHide (100);
	}
	
	private void toggle () {
		if (mVisible) {
			hide ();
		} else {
			show ();
		}
	}
	
	private void hide () {
		// Hide UI first
		ActionBar actionBar = getSupportActionBar ();
		if (actionBar != null) {
			actionBar.hide ();
		}
		mControlsView.setVisibility (View.GONE);
		mVisible = false;
		
		// Schedule a runnable to remove the status and navigation bar after a delay
		mHideHandler.removeCallbacks (mShowPart2Runnable);
		mHideHandler.postDelayed (mHidePart2Runnable, UI_ANIMATION_DELAY);
	}

	@SuppressLint("InlinedApi")
	private void show () {
		// Show the system bar
		mContentView.setSystemUiVisibility (View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
				| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
		mVisible = true;
		
		// Schedule a runnable to display UI elements after a delay
		mHideHandler.removeCallbacks (mHidePart2Runnable);
		mHideHandler.postDelayed (mShowPart2Runnable, UI_ANIMATION_DELAY);
	}
	
	/**
	 * Schedules a call to hide() in [delay] milliseconds, canceling any
	 * previously scheduled calls.
	 */
	private void delayedHide (int delayMillis) {
		mHideHandler.removeCallbacks (mHideRunnable);
		mHideHandler.postDelayed (mHideRunnable, delayMillis);
	}
}

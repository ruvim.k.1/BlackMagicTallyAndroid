package com.sccsalem.video.blackmagic_tally_client;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Ruvim Kondratyev on 8/21/2017.
 */

public class TallyStreamService extends Service {
	private static final String TAG = "TallyStreamService"; 
	TallyClient mTallyClient = null; 
	
	static final String BROADCAST_UPDATE = "com.sccsalem.video.TallyClient.TallyStreamService"; 
	static final String DATA_MESSAGE = "message"; 
	
	public TallyStreamService () {
		super ();
	}
	
	@Override
	public void onCreate () {
		super.onCreate ();
	}
	
	@Override
	public void onDestroy () {
		super.onDestroy ();
	}
	
	@Nullable
	@Override
	public IBinder onBind (Intent intent) {
		Log.i (TAG, "Starting audio ... ");
		mTallyClient = new TallyClient (intent.getExtras ().getString ("host")); 
		(new Thread () {
			@Override public void run () {
				mTallyClient.connect ();
				byte msg [] = new byte [4]; 
				int bRead;
				int retries = 0;
				while (retries < 8) {
					Log.d (TAG, "Trying to connect ... "); 
					sendMessage (null); // Connecting ... 
					try {
						mTallyClient.connect (); 
						if (!mTallyClient.isConnected ()) continue; 
						while ((bRead = mTallyClient.inputStream.read (msg, 0, 4)) > 0) { 
							if (bRead < 0) break; 
							if (bRead == 4) { 
								sendMessage (msg); 
							} 
							if (retries > 0) retries = 0; 
						}
						retries++;
					} catch (IOException err) {
						err.printStackTrace ();
						retries++;
					}
				} 
			}
		}).start ();
		return null;
	}
	
	void sendMessage (byte msg []) { 
		Intent message = new Intent (BROADCAST_UPDATE); 
		if (msg != null) 
			message.putExtra (DATA_MESSAGE, msg); 
		LocalBroadcastManager.getInstance (this) 
				.sendBroadcast (message); 
	} 
	
	@Override
	public boolean onUnbind (Intent intent) {
		Log.i (TAG, "Stopping audio ... ");
		if (mTallyClient != null) try {
			mTallyClient.disconnect ();
		} catch (IOException err) {
			err.printStackTrace ();
		}
		return super.onUnbind (intent);
	}
	
	@Override
	public void onRebind (Intent intent) {
		super.onRebind (intent);
	}
}

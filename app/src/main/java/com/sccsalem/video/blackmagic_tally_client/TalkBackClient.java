package com.sccsalem.video.blackmagic_tally_client;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by Ruvim Kondratyev on 8/21/2017.
 */

public class TalkBackClient {
	private static final String TAG = "TalkBackClient";
	
	private Socket socket = null;
	protected InputStream inputStream = null;
	protected OutputStream outputStream = null;
	private String host = "";
	protected int port = 50514;
	boolean mDisconnectCalled = false;
	
	
	
	public TalkBackClient (String host) {
		this.host = host;
	}
	public void connect () throws IOException {
		mDisconnectCalled = false;
		if (socket == null) {
			socket = new Socket (host, port);
			inputStream = socket.getInputStream ();
			outputStream = socket.getOutputStream ();
		} else {
			if (inputStream != null)
				inputStream.close ();
			if (outputStream != null)
				outputStream.close ();
			socket.close ();
			socket = new Socket (host, port);
			inputStream = socket.getInputStream ();
			outputStream = socket.getOutputStream ();
		}
	}
	public void disconnect () throws IOException {
		if (mDisconnectCalled) return;
		mDisconnectCalled = true;
		if (socket != null) {
			inputStream.close ();
			outputStream.close ();
			socket.close ();
		}
		if (mAudioRecorder != null)
			freeRecorder ();
	}
	public boolean tryReconnect () {
		try {
			connect ();
		} catch (IOException err) {
			err.printStackTrace ();
		}
		return socket.isConnected ();
	}
	public boolean isConnected () {
		return socket != null && socket.isConnected ();
	}
	private static void sleep (long ms) {
		try {
			Thread.sleep (ms);
		} catch (InterruptedException err) {
			err.printStackTrace ();
		}
	}
	public void resetPlayer (int new_sample_rate) {
//		mAudioPlayer.SAMPLE_RATE = new_sample_rate;
//		mAudioPlayer.reset ();
	}
	synchronized void freeRecorder () {
		if (mAudioRecorder == null) return;
		try {
			mAudioRecorder.stop ();
		} catch (IllegalStateException err) {
			// It's okay. 
		}
		mAudioRecorder.release ();
		mAudioRecorder = null;
	}
	int audioSource = MediaRecorder.AudioSource.DEFAULT;
	int sampleRate = 44100;
	int channelConfig = AudioFormat.CHANNEL_IN_MONO;
	int audioFormat = AudioFormat.ENCODING_PCM_16BIT;
	int bufferSize = 0;
	byte [] buffer = null;
	int recordingBuffer = 0;
	final int BUF_COUNT = 24;
	AudioRecord mAudioRecorder = null;
	public void record () throws IOException {
		if (outputStream == null) return; 
		// Do the work for recording the data: 
		int bRead;
		bufferSize = AudioRecord.getMinBufferSize (sampleRate, channelConfig, audioFormat);
		buffer = new byte [bufferSize * BUF_COUNT];
		mAudioRecorder = new AudioRecord (audioSource, sampleRate, channelConfig, audioFormat, bufferSize);
		mAudioRecorder.startRecording ();
		do {
			bRead = mAudioRecorder.read (buffer, recordingBuffer * bufferSize, bufferSize);
			if (bRead <= 0) {
				if (mDisconnectCalled)
					break; // It's okay. 
				throw new IOException ("Could not read any bytes from the microphone. ");
			}
			outputStream.write (buffer, recordingBuffer * bufferSize, bufferSize);
			outputStream.flush ();
			recordingBuffer = (recordingBuffer + 1) % BUF_COUNT;
		} while (!mDisconnectCalled); 
	} 
}

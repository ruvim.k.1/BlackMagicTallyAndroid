package com.sccsalem.video.blackmagic_tally_client;

import android.content.Context;

/**
 * Created by Ruvim Kondratyev on 9/10/2017.
 */

public class SteadySignal { 
	AudioPlayer player = null; 
	short buffer [] = new short [1024]; 
	boolean mNeedStop = false; 
	public SteadySignal (short value) { 
		for (int i = 0; i < buffer.length; i++) 
			buffer[i] = (short) (value * Math.sin (i / 256)); 
//		buffer[i] = value; 
	} 
	void sleep (long ms) { 
		try { 
			Thread.sleep (ms); 
		} catch (InterruptedException err) { 
			// Do nothing. 
		} 
	} 
	public void init (Context context) { 
		if (player != null) { 
			player.checkMute (context); 
			return; 
		} 
		player = new AudioPlayer (); 
		player.init (); 
		player.checkMute (context); 
	} 
	public void play () { 
		mNeedStop = false; 
		if (player == null) { 
			player = new AudioPlayer (); 
			player.init (); 
		} 
		while (!mNeedStop) { 
			player.play (buffer, buffer.length); 
		} 
		player.close (); 
		player = null; 
	} 
	public void stop () { 
		mNeedStop = true; 
	} 
} 

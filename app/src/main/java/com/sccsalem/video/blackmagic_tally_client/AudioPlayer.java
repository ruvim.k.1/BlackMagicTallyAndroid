package com.sccsalem.video.blackmagic_tally_client;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRouting;
import android.media.AudioTrack;
import android.util.Log;

import java.util.Vector;

/**
 * Created by Ruvim Kondratyev on 8/14/2017.
 */

// Useful help link: 
	// https://stackoverflow.com/questions/39741926/android-audiotrack-write-returns-after-playing-the-whole-buffer

public class AudioPlayer { 
	static final String TAG = "AudioPlayer"; 
	
	boolean mIsInitialized = false; 
	int bufSize; 
	AudioTrack audioTrack; 
	
	boolean mIsMuted = false; 
	boolean mMuteOnSpeaker = true; 
	
	int SAMPLE_RATE = 44100; 
	
	void reset () {
		close ();
		init ();
	}
	void close () {
		try {
			if (audioTrack != null)
				audioTrack.stop ();
		} catch (IllegalStateException err) {
			
		}
		if (audioTrack != null)
			audioTrack.release ();
		mIsInitialized = false;
	}
	void init () {
		if (mIsInitialized) return;
		bufSize = AudioTrack.getMinBufferSize (SAMPLE_RATE,
				AudioFormat.CHANNEL_OUT_MONO,
				AudioFormat.ENCODING_PCM_16BIT);
		audioTrack = new AudioTrack (AudioManager.STREAM_MUSIC,
											SAMPLE_RATE, 
											AudioFormat.CHANNEL_OUT_MONO,
											AudioFormat.ENCODING_PCM_16BIT,
											bufSize,
											AudioTrack.MODE_STREAM);
		try {
			audioTrack.play ();
		} catch (IllegalStateException err) {
			err.printStackTrace ();
		} 
		mIsInitialized = true;
	}
	
	public void play (byte [] stream, int len) { 
		if (!mIsInitialized) init (); 
		audioTrack.write (stream, 0, len); 
	} 
	public void play (short [] stream, int len) { 
		if (!mIsInitialized) init (); 
		audioTrack.write (stream, 0, len); 
	} 
	
	AudioManager audioManager = null; 
	void checkMute (Context context) { 
		if (audioTrack == null) return; 
		if (audioManager == null) {
			audioManager = (AudioManager) context.getSystemService (Context.AUDIO_SERVICE);
		}
		try {
			if (mIsMuted && !mMuteOnSpeaker) {
				audioTrack.setStereoVolume (1, 1);
				mIsMuted = false;
			} else if (mMuteOnSpeaker) {
				AudioManager manager = audioManager; 
				boolean headphoneOn = manager.isBluetoothScoOn () ||
											  manager.isBluetoothA2dpOn () ||
											  manager.isWiredHeadsetOn ();
				boolean needMute = !headphoneOn;
				if (!needMute) audioTrack.setStereoVolume (1, 1);
				else {
					audioTrack.setStereoVolume (0, 0);
				}
				mIsMuted = needMute; 
			}
		} catch (IllegalStateException err) {
			err.printStackTrace ();
		} 
	} 
} 

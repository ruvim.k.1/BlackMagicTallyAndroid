package com.sccsalem.video.blackmagic_tally_client;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Ruvim Kondratyev on 8/16/2017.
 */

public class SoundStreamService extends Service { 
	private static final String TAG = "SoundStreamService"; 
	AudioStreamClient mSoundClient = null; 
	SteadySignal signal = new SteadySignal ((short) (32282)); 
	
	public SoundStreamService () {
		super ();
	}
	
	@Override
	public void onCreate () {
		super.onCreate ();
	}
	
	@Override
	public void onDestroy () {
		super.onDestroy ();
	}
	
	@Nullable
	@Override
	public IBinder onBind (Intent intent) {
		Log.i (TAG, "Starting audio ... "); 
//		mSoundClient = new AudioStreamClient (intent.getExtras ().getString ("host")); 
		(new Timer ()).scheduleAtFixedRate (new TimerTask () { 
			@Override public void run () { 
				signal.player.checkMute (SoundStreamService.this); 
			} 
		}, 500, 1000); 
		(new Thread () { 
			@Override public void run () {
//				int retries = 0; 
//				while (retries < 8) { 
//					Log.d (TAG, "Trying to connect to sound ... "); 
//					try { 
//						mSoundClient.connect (); 
//						if (!mSoundClient.isConnected ()) continue; 
//						mSoundClient.play (); 
//						retries++; 
//					} catch (IOException err) { 
//						err.printStackTrace (); 
//						retries++; 
//					} 
//				} 
				signal.init (SoundStreamService.this); 
				signal.play (); 
			} 
		}).start (); 
		return null;
	}
	
	@Override
	public boolean onUnbind (Intent intent) {
		Log.i (TAG, "Stopping audio ... "); 
//		if (mSoundClient != null) try {
//			mSoundClient.disconnect ();
//		} catch (IOException err) { 
//			err.printStackTrace (); 
//		} 
		signal.stop (); 
		return super.onUnbind (intent);
	}
	
	@Override
	public void onRebind (Intent intent) {
		super.onRebind (intent);
	}
}

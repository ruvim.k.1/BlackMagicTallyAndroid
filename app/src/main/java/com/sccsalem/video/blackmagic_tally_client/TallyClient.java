package com.sccsalem.video.blackmagic_tally_client;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.Vector;

/**
 * Created by ruvim on 8/7/17.
 */

public class TallyClient { 
	private static final String TAG = "TallyClient"; 
	
	private Socket socket = null; 
	protected InputStream inputStream = null; 
	private String host = ""; 
	protected int port = 50512; 
	boolean mDisconnectCalled = false; 
	
	public TallyClient (String host) { 
		this.host = host; 
	} 
	public void connect () { 
		try { 
			if (socket == null) { 
				socket = new Socket (host, port); 
				inputStream = socket.getInputStream (); 
			} else { 
				if (inputStream != null) 
					inputStream.close (); 
				socket.close (); 
				socket = new Socket (host, port); 
				inputStream = socket.getInputStream (); 
			} 
		} catch (IOException err) { 
			err.printStackTrace (); 
		} 
	} 
	public void disconnect () throws IOException { 
		mDisconnectCalled = true; 
		if (socket != null) { 
			if (socket.isConnected ()) { 
				inputStream.close (); 
				socket.close (); 
			} 
		} 
	} 
	public boolean isConnected () { 
		return socket != null && socket.isConnected (); 
	} 
	public int available () { 
		try { 
			return inputStream != null ? inputStream.available () : 0; 
		} catch (IOException err) { 
			err.printStackTrace (); 
			return 0; 
		} 
	} 
	public byte [] readMessage () { 
		if (inputStream == null) return null; 
		try { 
			byte [] buffer = new byte [4]; 
			int bRead = inputStream.read (buffer, 0, 4); 
			if (bRead <= 0) return null; 
			return buffer; 
		} catch (IOException err) { 
			err.printStackTrace (); 
			return null; 
		} 
	} 
} 

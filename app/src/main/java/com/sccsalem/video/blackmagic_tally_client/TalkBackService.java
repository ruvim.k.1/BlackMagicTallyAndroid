package com.sccsalem.video.blackmagic_tally_client;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.FileDescriptor;
import java.io.IOException;
import java.net.SocketException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Ruvim Kondratyev on 8/21/2017.
 */

public class TalkBackService extends Service {
	private static final String TAG = "TalkBackService"; 
	TalkBackClient mTalkbackClient = null;
	
	public TalkBackService () {
		super ();
	}
	
	@Override
	public void onCreate () {
		super.onCreate ();
	}
	
	@Override
	public void onDestroy () {
		super.onDestroy ();
	}
	
	@Nullable
	@Override
	public IBinder onBind (Intent intent) {
		Log.i (TAG, "Starting audio ... ");
		mTalkbackClient = new TalkBackClient (intent.getExtras ().getString ("host")); 
		(new Thread () {
			@Override public void run () {
				int retries = 0; 
				while (retries < 1 && !mTalkbackClient.mDisconnectCalled) {
					Log.d (TAG, "Trying to connect to sound ... "); 
					try { 
						try { 
							mTalkbackClient.connect (); 
							if (!mTalkbackClient.isConnected ()) continue; 
							mTalkbackClient.record (); 
							retries++; 
						} catch (SocketException err) { 
							err.printStackTrace (); 
							mTalkbackClient.freeRecorder (); 
							retries++; 
						} 
					} catch (IOException err) {
						err.printStackTrace (); 
						mTalkbackClient.freeRecorder (); 
						retries++;
					}
				} 
				mTalkbackClient.freeRecorder (); 
			}
		}).start ();
		return null; 
	}
	
	@Override
	public boolean onUnbind (Intent intent) {
		Log.i (TAG, "Stopping audio ... "); 
		if (mTalkbackClient != null) try { 
			mTalkbackClient.disconnect (); 
		} catch (IOException err) {
			err.printStackTrace ();
		}
		return super.onUnbind (intent);
	}
	
	@Override
	public void onRebind (Intent intent) {
		super.onRebind (intent);
	}
}
